$fn=180;
module DH_SlidingBlock()
{
    color("red")
    {
        difference()
        {
            translate([0,0,3])
            {
                cube([31.8,17,6], center= true);
            }
            for(i=[-1,1])
            {
                for(j=[-1,1])
                {
                    translate([6.5*i,6*j,-0.001])
                    {
                        cylinder(d=2.5,h=6.002);
                    }
                }
            }
            translate([5,0,6])
            {
                rotate([0,-90,0])
                {
                    cylinder(d=6,h=22.5);
                }
            }
        }
    }
}

DH_SlidingBlock();