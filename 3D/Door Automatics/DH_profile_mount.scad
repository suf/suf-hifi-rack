$fn=180;

module DH_profile_mount()
{
    color("Red")
    {
        difference()
        {
            hull()
            {
                translate([-20,0,0])
                    cube([40,20,0.001]);
                translate([-20,20,20])
                    cube([40,0.001,0.001]);
            }
            for(i=[-1,1])
            {
                for(j=[0,1])
                {
                    translate([0,20.001*j,0])
                    {
                        rotate([90*j,0,0])
                        {
                            translate([10*i,10,-0.001])
                                cylinder(d=6.5,h=20);
                            translate([10*i,10,2.5])
                                cylinder(d=12,h=20);
                        }
                    }
                }
            }
        }
    }
}

DH_profile_mount();