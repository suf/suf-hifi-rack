$fn=180;
module DH_LeadScrewNutHolder()
{
    color("red")
    {
        for(j=[-1,1])
        {
            translate([0,23*j,0])
            {
                rotate([90-90*j,0,0])
                {
                    difference()
                    {
                        union()
                        {
                            hull()
                            {
                                cube([150,3,30], center=true);
                                translate([0,-1.5,0])
                                    cube([150,6,6], center=true);
                            }
                            translate([-73.5,-1.5,0])
                                cube([3,6,30], center=true);
                        }
                        for(i=[-1,1])
                        {
                            translate([-72.001,-2.5,13*i])
                                rotate([0,90,0])
                                    cylinder(d1=7.5,d2=1,h=20.002);
                            translate([-75.001,-1.5,12*i])
                                rotate([0,90,0])
                                    cylinder(d=2.8,h=3.002);
                        }
                    }
                }
            }
        }
        translate([75,0,0])
            cube([5,49,30],center=true);
    }
}
translate([0,0,15])
    DH_LeadScrewNutHolder();
