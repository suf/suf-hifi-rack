// NEMA 17 Motor design
$fn=100;

module cube_cutcorner(width, depth, height, cut)
{
    translate([0,0,height/2])
    {
        intersection()
        {
            cube([width,depth,abs(height)], center = true);
            rotate([0,0,45])
            {
                cube([(width-cut) * 2 * sin(45),
                    (depth -cut) * 2 * sin(45),abs(height)], center = true);
            }
        }
    }
}

module Nema17(mheight)
{
    color("lightgray")
    {
        cylinder(h=2, r=11);
        cylinder(h=22, r=2.5);
        difference()
        {
            cube_cutcorner(42.3,42.3,-7.5,3);
            for(i=[0:3])
            {
                rotate([0, 0, 90*i])
                {
                    translate([15.5, 15.5, -5])
                    {
                        cylinder(h=5.001, r=1.5);
                    }
                }
            }
        }
    }
    color("black")
    {
        translate([0,0,-7.501])
        {
            cube_cutcorner(42.3,42.3,17.5-mheight,5);
        }
    }
    color("lightgray")
    {
        translate([0,0,mheight * -1])
        {
            cube_cutcorner(42.3,42.3,10,3);
        }
    }
}

Nema17(34);