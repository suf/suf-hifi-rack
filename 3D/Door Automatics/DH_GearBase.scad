$fn=180;

module DH_GearBase()
{
    color("Red")
    {
        difference()
        {
            translate([10,-25,0])
            {
                cube([60,50,14]);
            }
            /*
            union()
            {
                minkowski()
                {
                    translate([5,-20,0])
                    {
                        cube([50,40,13.999]);
                    }
                    cylinder(d=10,h=0.001);
                }
            }
            */
            translate([-0.001,-25.001,-0.001])
            {
                cube([10.001,25.001,14.002]);
            }
            
            translate([14.75,10,14])
            {
                rotate([0,90,0])
                {
                    cylinder(d=10.5,h=15.5);
                }
            }
            translate([9.999,10,14])
            {
                rotate([0,90,0])
                {
                    cylinder(d=7,h=60.002);
                }
            }
            for(i=[-1,1])
            {
                for(j=[-1,1])
                {
                    translate([35+i*20,20*j,-0.001])
                    {
                        cylinder(d=4.5,h=14.002);
                    }
                }
            }
            
        }

    }
}

module DH_SlidingTop()
{
    color("Red")
    {
        difference()
        {
            translate([10,-25,0])
            {
                cube([60,50,8]);
            }
            
            translate([14.75,10,0])
            {
                rotate([0,90,0])
                {
                    cylinder(d=10.5,h=15.5);
                }
            }
            translate([9.999,10,0])
            {
                rotate([0,90,0])
                {
                    cylinder(d=7,h=60.002);
                }
            }
            for(i=[-1,1])
            {
                for(j=[-1,1])
                {
                    translate([35+i*20,20*j,-0.001])
                    {
                        cylinder(d=4.5,h=14.002);
                    }
                }
            }
            
        }

    }
}

module DH_MountBase()
{
    color("Red")
    {
        difference()
        {
            translate([0,-25,0])
            {
                cube([70,50,8]);
            }
            for(i=[-1,1])
            {
                for(j=[-1,1])
                {
                    translate([25+i*21,21*j,-0.001])
                    {
                        cylinder(d=4.5,h=14.002);
                    }
                }
            }
            translate([25,0,5])
                cylinder(d=30,h=10);
        }

    }
    
}

module DH_CrossProfileMount()
{
    color("Red")
    {
        difference()
        {
            union()
            {
                minkowski()
                {
                    translate([5,-20,0])
                    {
                        cube([50,40,7.999]);
                    }
                    cylinder(d=10,h=0.001);
                }
            }
           
            translate([15,-10,0])
            {
                rotate([0,90,0])
                {
                    cylinder(d=10.5,h=15.5);
                }
            }
            translate([9.999,-10,0])
            {
                rotate([0,90,0])
                {
                    cylinder(d=7,h=60.002);
                }
            }
            for(i=[-1,1])
            {
                for(j=[-1,1])
                {
                    translate([35+i*20,20*j,-0.001])
                    {
                        cylinder(d=4.5,h=14.002);
                    }
                }
            }

        }

    }
}



DH_GearBase();

translate([0,0,25])
    DH_SlidingTop();

translate([0,0,45])
    DH_MountBase();


/*
module DH_GearBase()
{
    color("Red")
    {
        difference()
        {
            union()
            {
                minkowski()
                {
                    translate([3,0,7])
                    {
                        cube([51,31,13.999],center=true);
                    }
                    cylinder(d=11,h=0.001);
                }

                
                // translate([30,-5,0])
                // {
                //    cube([10,10,14]);
                // }
                
            }
            translate([-7.75,-10,14])
            {
                rotate([0,90,0])
                {
                    cylinder(d=10.5,h=15.5);
                }
            }
            translate([-28.001,-10,14])
            {
                rotate([0,90,0])
                {
                    cylinder(d=7,h=62.002);
                }
            }

            
            
            for(i=[-1,1])
            {
                for(j=[-1,1])
                {
                    translate([15.5*i,15.5*j,-0.001])
                    {
                        cylinder(d=3.5,h=14.002);
                    }
                }
            }
            
        }

    }
}
*/