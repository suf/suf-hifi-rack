$fn=180;
module DH_BlockSpacer()
{
    color("red")
    {
        difference()
        {
            translate([0,0,2.5])
            {
                cube([25,30,5], center= true);
            }
            for(i=[-1,1])
            {
                for(j=[-1,1])
                {
                    translate([7.5*i,10*j,-0.001])
                    {
                        cylinder(d=4.5,h=5.002);
                    }
                }
            }
            
        }
    }
}

DH_BlockSpacer();