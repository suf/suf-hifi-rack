$fn=180;

Size_ZH8 = 0;
Size_ZH8M = 1;

ZH_D = 0;
ZH_L = 1;
ZH_D1 = 2;
ZH_H = 3;
ZH_T = 4;
ZH_W = 5;
ZH_P1 = 6;
ZH_d1 = 7;

ZH_param = [
[8,16,12,28,5,16,20,3.5],
[8,16,12,28,5,16,20,4.5]
];

module ZH(type)
{
    D = ZH_param[type][ZH_D];
    L = ZH_param[type][ZH_L];
    D1 = ZH_param[type][ZH_D1];
    H = ZH_param[type][ZH_H];
    T = ZH_param[type][ZH_T];
    W = ZH_param[type][ZH_W];
    P1 = ZH_param[type][ZH_P1];
    d1 = ZH_param[type][ZH_d1];
    color("lightgray")
    {
        difference()
        {
            union()
            {
                intersection()
                {
                    cylinder(d=H,h=T);
                    translate([0,0,0.5 * T])
                        cube([W,H+0.002,T], center=true);
                }
                cylinder(d=D1,h=L);
            }
            translate([0,0,-0.001])
                cylinder(d=D,h=L+0.002);
            for(i=[-1,1])
            {
                translate([0,i*P1*0.5,-0.001])
                    cylinder(d=d1,h=T+0.002);
            }
        }
    }
}

module ZH8()
{
    ZH(Size_ZH8);
}

ZH8();