$fn=100;

SC6 = 0;
SC8 = 1;
SC10 = 2;
SC12 = 3;

SCxxUU_T = 0;
SCxxUU_h = 1;
SCxxUU_E = 2;
SCxxUU_W = 3;
SCxxUU_W1 = 4;
SCxxUU_W2 = 5;
SCxxUU_W3 = 6;
SCxxUU_L = 7;
SCxxLUU_L = 8;
SCxxUU_F = 9;
SCxxUU_G = 10;
SCxxUU_B = 11;
SCxxUU_C = 12;
SCxxLUU_C = 13;
SCxxUU_K = 14;
SCxxUU_S1 = 15;
SCxxUU_L1 = 16;
LMxxUU_d = 17;
LMxxUU_D = 18;
LMxxUU_L = 19;
LMxxLUU_L = 20;

SCxxUU_Params = [
[ 6, 9,15,30,28,14, 9,25,48,18,15,20,15,36, 5, 4, 8, 6,12,19,35],
[ 6,11,17,34,31,15, 7,30,58,22,18,24,18,42, 5, 4, 8, 8,15,24,45],
[ 8,13,20,40,38,17,10,35,68,26,21,28,21,45, 6, 5,12,10,19,29,55],
[ 8,15,21,42,40,22,13.5,36,70,28,24,30.5,26,50,5.75,5,12,12,21,30,57]
];

module SCxxUU(type,isLong)
{
    sc_W  = SCxxUU_Params[type][SCxxUU_W];
    sc_W1 = SCxxUU_Params[type][SCxxUU_W1];
    sc_W2 = SCxxUU_Params[type][SCxxUU_W2];
    sc_W3 = SCxxUU_Params[type][SCxxUU_W3];
    sc_L  = SCxxUU_Params[type][isLong ? SCxxLUU_L : SCxxUU_L];
    sc_T  = SCxxUU_Params[type][SCxxUU_T];
    sc_L1 = SCxxUU_Params[type][SCxxUU_L1];
    sc_G  = SCxxUU_Params[type][SCxxUU_G];
    sc_F  = SCxxUU_Params[type][SCxxUU_F];
    lm_D  = SCxxUU_Params[type][LMxxUU_D];
    sc_h  = SCxxUU_Params[type][SCxxUU_h];
    lm_d  = SCxxUU_Params[type][LMxxUU_d];
    sc_C  = SCxxUU_Params[type][isLong ? SCxxLUU_C : SCxxUU_C];
    sc_K  = SCxxUU_Params[type][SCxxUU_K];
    sc_S1 = SCxxUU_Params[type][SCxxUU_S1];
    lm_L = SCxxUU_Params[type][isLong ? LMxxLUU_L : LMxxUU_L];
    sc_B = SCxxUU_Params[type][SCxxUU_B];
    
    color("lightgray")
    {
        difference()
        {
            // Body
            translate([sc_L*-0.5,sc_W*-0.5,0])
            {
                rotate([90,0,90])
                {
                    linear_extrude(height=sc_L)
                    {
                        polygon(points=[[0,0],
                                        [sc_W,0],
                                        [sc_W,sc_T],
                                        [(sc_W+sc_W1)/2,sc_L1],
                                        [(sc_W+sc_W1)/2,sc_G],
                                        [(sc_W+sc_W2)/2,sc_G],
                                        [(sc_W+sc_W3)/2,sc_F],
                                        [(sc_W-sc_W3)/2,sc_F],
                                        [(sc_W-sc_W2)/2,sc_G],
                                        [(sc_W-sc_W1)/2,sc_G],
                                        [(sc_W-sc_W1)/2,sc_L1],
                                        [0,sc_T]]);
                    }
                }
            }
            // Bearing hole
            
            translate([0,0,sc_h])
            {
                rotate([0,90,0])
                {
                    translate([0,0,sc_L*-0.5 - 0.001])
                    {
                        cylinder(d=lm_D,h=sc_L +0.002);
                    }
                }
            }
            // screw hole
            for(i=[0,1])
            {
                mirror([0,i,0])
                {
                    for(j=[0,1])
                    {
                        mirror([j,0,0])
                        {
                            translate([sc_C*0.5,sc_B*0.5,-0.001])
                            {
                                cylinder(d=sc_S1,h=sc_G+0.002);
                            }
                        }
                    }
                }
            }
        }
    }
    // Bearing
    color([0.15,0.15,0.15])
    {
        translate([0,0,sc_h])
        {
            rotate([0,90,0])
            {
                translate([0,0,lm_L*-0.5])
                {
                    difference()
                    {
                        cylinder(d=lm_D,h=lm_L);
                        translate([0,0,-0.001])
                        {
                            cylinder(d=lm_d,h=lm_L+0.002);
                        }
                    }
                }
            }
        }
    }
}

module SC6UU()
{
    SCxxUU(SC6,false);
}

module SC6LUU()
{
    SCxxUU(SC6,true);
}

module SC8UU()
{
    SCxxUU(SC8,false);
}

module SC8LUU()
{
    SCxxUU(SC8,true);
}

module SC10UU()
{
    SCxxUU(SC10,false);
}

module SC10LUU()
{
    SCxxUU(SC10,true);
}

module SC12UU()
{
    SCxxUU(SC12,false);
}

module SC12LUU()
{
    SCxxUU(SC12,true);
}

SC10LUU();








































