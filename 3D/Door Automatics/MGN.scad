$fn=180;
MGN7 = 0;
MGN9 = 1;
MGN12 = 2;
MGN15 = 3;

MGN_H = 0;
MGN_H1 = 1;
MGN_N = 2;
MGN_W = 3;
MGN_B = 4;
MGN_B1 = 5;
MGN_C_C = 6;
MGN_C_L1 = 7;
MGN_C_L = 8;
MGN_H_C = 9;
MGN_H_L1 = 10;
MGN_H_L = 11;
MGN_G = 12;
MGN_H2 = 13;
MGN_Wr = 14;
MGN_Hr = 15;
MGN_D = 16;
MGN_h = 17;
MGN_d = 18;
MGN_P = 19;
MGN_E = 20;

MGN_param = [
[ 8  , 1.5, 5  ,17  ,12  , 2.5, 8  ,13.5,22.5,13  ,21.8,30.8, 0  , 1.5, 7  , 4.8, 4.2, 2.3, 2.4,15  , 5  ],
[10  , 2  , 5.5,20  ,15  , 2.5,10  ,18.9,28.9,16  ,29.9,39.9, 0  , 1.8, 9  , 6.5, 6  , 3.5, 3.5,20  , 7.5],
[13  , 3  , 7.5,27  ,20  , 3.5,15  ,21.7,34.7,20  ,32.4,45.4, 0  , 2.5,12  , 8  , 6  , 4.5, 3.5,25  ,10  ],
[16  , 4  , 8.5,32  ,25  , 3.5,20  ,26.7,42.1,25  ,43.4,58.8, 4.5, 3  ,15  ,10  , 6  , 4.5, 3.5,40  ,15  ]
];

module MGN_Rail(type, length)
{
    E = MGN_param[type][MGN_E];
    P = MGN_param[type][MGN_P];
    color("lightgray")
    {
        difference()
        {
            translate([0,0,MGN_param[type][MGN_Hr] * 0.5])
            {
                cube([length,MGN_param[type][MGN_Wr],MGN_param[type][MGN_Hr]], center = true);
            }
            for(i=[-1,1])
            {
                translate([-0.001,(MGN_param[type][MGN_Wr] * 0.5)*i,
                    (MGN_param[type][MGN_Hr] + MGN_param[type][MGN_H1]) * 0.5])
                {
                    rotate([0,90,0])
                    {
                        translate([0,0,(length+0.02)*-0.5])
                        {
                            cylinder(d=MGN_param[type][MGN_H1], h=length+0.02);
                        }
                    }
                }
            }
            for(i=[E:P:length])
            {
                translate([i - length/2, 0, -0.001])
                {
                    cylinder(d=MGN_param[type][MGN_d], h=MGN_param[type][MGN_Hr]);
                }
                translate([i - length/2, 0, MGN_param[type][MGN_Hr] - MGN_param[type][MGN_h]])
                {
                    cylinder(d=MGN_param[type][MGN_D], h=MGN_param[type][MGN_h]+0.001);
                }
            }
        }
    }
}

module MGN7R(length)
{
    MGN_Rail(MGN7,length);
}

module MGN9R(length)
{
    MGN_Rail(MGN9,length);
}


module MGN12R(length)
{
    MGN_Rail(MGN12,length);
}

module MGN_Car(type, wide)
{
    car_len = MGN_param[type][wide ? MGN_H_L : MGN_C_L];
    car_metal_len = MGN_param[type][wide ? MGN_H_L1 : MGN_C_L1];
    difference()
    {
        union()
        {
            color("lightgray")
                translate([0,0,(MGN_param[type][MGN_H] + MGN_param[type][MGN_H1])/ 2])
                    cube([MGN_param[type][wide ? MGN_H_L1 : MGN_C_L1],
                          MGN_param[type][MGN_W],
                          MGN_param[type][MGN_H] - MGN_param[type][MGN_H1] ], center = true);
            for(i=[0,1])
            {
                mirror([i,0,0])
                {
                    color("green")
                        translate([(car_len + car_metal_len) / 4 - 0.5,0,(MGN_param[type][MGN_H] + MGN_param[type][MGN_H1])/ 2])
                            cube([(car_len - car_metal_len) / 2 - 1,
                                  MGN_param[type][MGN_W],
                                  MGN_param[type][MGN_H] - MGN_param[type][MGN_H1] ], center = true);
                    color("red")
                        translate([(car_len) / 2 - 0.5,0,(MGN_param[type][MGN_H] + MGN_param[type][MGN_H1])/ 2])
                            cube([1,
                                  MGN_param[type][MGN_W],
                                  MGN_param[type][MGN_H] - MGN_param[type][MGN_H1] ], center = true);
                }
            }
        }
        translate([0,0,(MGN_param[type][MGN_Hr] + 1)/ 2])
        cube([MGN_param[type][wide ? MGN_H_L : MGN_C_L] + 0.002,
              MGN_param[type][MGN_Wr] + 1,
              MGN_param[type][MGN_Hr] + 1], center = true);
        for(i=[-1,1])
        {
            for(j=[-1,1])
            {
                translate([(MGN_param[type][wide ? MGN_H_C : MGN_C_C] / 2) * i,
                           (MGN_param[type][MGN_B] / 2) * j,
                           MGN_param[type][MGN_H1] * 2 + 0.001])
                    cylinder(d= MGN_param[type][MGN_d], h= MGN_param[type][MGN_H] - MGN_param[type][MGN_H1] * 2);
            }
        }
    }
}

module MGN7C()
{
    MGN_Car(MGN7, false);
}
module MGN7H()
{
    MGN_Car(MGN7, true);
}

module MGN9C()
{
    MGN_Car(MGN9, false);
}
module MGN9H()
{
    MGN_Car(MGN9, true);
}

module MGN12H()
{
    MGN_Car(MGN12, true);
}

MGN_Rail(MGN12,200);

MGN12H();