$fn=180;
use <profile.scad>
use <MGN.scad>
use <GT2.scad>
use <Nema17.scad>
use <opto_endstop_v1.scad>

opening=0;


profile_length = 300;
/*
color("aqua",0.3)
{
    translate([0,0,35])
    {
        cube([440,50,0.001],center=true);
    }
}
*/
for(i=[-1,1])
{
    color("silver")
    {
        translate([profile_length*-0.5,12.5*i,10])
        {
            rotate([0,90,0])
            {
                alu_profile(1,1,profile_length);
            }
        }
    }
    translate([0,12.5*i,20])
    {
        MGN9R(300);
        translate([((profile_length-39.9)/2 - opening)*i,0,0])
        {
            MGN9H();
        }
    }
    color("silver")
    {
        translate([((profile_length-39.9)/2 - opening)*i,12.5*i,38])
        rotate([0,-90*i,0])
        {
            cylinder(d=6,h=350);
        }
    }
    translate([profile_length*0.5*i,-7.5*i,50])
    {
        rotate([0,0,90*i])
        {
            opto_endstop();
        }
    }

}

translate([-200,0,33.75])
{
    GT2_Gear_NoTeeth_20();
}

translate([200,0,27])
{
    GT2_Pulley_20T();
}


/*
translate([((profile_length-39.9)/2)*-1,-12.5,34])
{
    cube([20,20,8], center=true);
}
*/

translate([200,0,21])
{
    Nema17(34);
}

translate([0,0,38])
GT2_Belt(-200,0,200,0,6,17);