$fn=180;
module DH_GearHolder()
{
    color("red")
    {
        difference()
        {
            translate([0,0,1.5])
            {
                cube([42,42,3], center=true);
            }
            translate([0,0,-0.001])
            {
                cylinder(d=4.5,h=3.002);
            }
            for(i=[-1,1])
            {
                for(j=[-1,1])
                {
                    translate([15.5*i,15.5*j,-0.001])
                    {
                        cylinder(d=3.5,h=3.002);
                    }
                }
            }
        }
    }
}

DH_GearHolder();