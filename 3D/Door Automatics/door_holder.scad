$fn=180;
use <SCxxUU.scad>
use <Nema17.scad>
use <MGN.scad>
use <HexSpacer.scad>
use <GT2.scad>
use <ls_nut.scad>
use <ZH.scad>

use <DH_GearHolder.scad>
use <DH_BlockSpacer.scad>
use <DH_SlidingBlock.scad>
use <DH_BeltHolder.scad>
use <DH_BeltSpanner.scad>
use <DH_LeadScrewNutHolder.scad>
use <DH_GearBase.scad>
use <DH_MotorMount.scad>

OPEN=130;

// Base build plate
translate([0,1.5,25])
{
    cube([420,3,50], center=true);
}

/*
translate([0,30,58.5])
{
    cube([440,60,3], center=true);
}
*/

// Sliding rod bearings
for(i=[-1,1])
{
    translate([197.5 * i,17,25 + i * 10])
    {
        LM6UU_copper();
    }
}

/*
// Sliding rod bearing mount plate
for(i=[-1,1])
{
    translate([175 * i,3,25 + i * 10])
    {
        rotate([-90,0,0])
        {
            DH_BlockSpacer();
        }
    }
}
*/

// Sliding rod
for(i=[-1,1])
{
    translate([(-135 + OPEN) * i,17,25 + i * 10])
    {
        rotate([0,90*i,0])
        {
            cylinder(d=6,h=360);
        }
    }
}

// Sliding motor
translate([-178,48,25])
{
    rotate([90,0,0])
    {
        Nema17(34);
    }
}

/*
// Opeing motor
translate([0,150,25])
{
    rotate([90,0,0])
    {
        Nema17(34);
    }
}
*/

// Sliding mechanism
for(i=[-1,1])
{
    translate([0,3,25 + i * 10])
    {
        rotate([-90,0,0])
        {
            MGN7R(300);
            translate([(-130+OPEN)*i,0,0])
            {
                MGN7H();
                translate([0,0,8])
                {
                    rotate([0,0,90 + 90*i])
                    {
                        DH_SlidingBlock();
                        translate([0,0,6])
                        {
                            DH_BeltHolder();
                        }
                        translate([0,0,16])
                        {
                            rotate([0,0,180])
                            {
                                DH_BeltSpanner();
                            }
                        }
                    }
                }
            }
        }
    }
}

/*
// between gear holder plates
for(i=[-1,1])
{
    for(j=[-1,1])
    {
        translate([170+i*15.5,31,25+j*15.5])
        {
            rotate([-90,0,0])
            {
                HexSpacer(5.5,3,12);
            }
        }
    }
}
*/
/*
// Sliding motor spacers
translate([-170+15.5,3,25+15.5])
{
    rotate([-90,0,0])
    {
        HexSpacer(5.5,3,45);
    }
}

translate([-170-15.5,3,25+15.5])
{
    rotate([-90,0,0])
    {
        HexSpacer(5.5,3,45);
    }
}

translate([-170+15.5,3,25-15.5])
{
    rotate([-90,0,0])
    {
        HexSpacer(5.5,3,45);
    }
}
*/
/*
// gear spacers
translate([170-15.5,3,25-15.5])
{
    rotate([-90,0,0])
    {
        HexSpacer(5.5,3,25);
    }
}
translate([170-15.5,3,25+15.5])
{
    rotate([-90,0,0])
    {
        HexSpacer(5.5,3,25);
    }
}
translate([170+15.5,3,25-15.5])
{
    rotate([-90,0,0])
    {
        HexSpacer(5.5,3,25);
    }
}
*/
// Gear and motor base
for(i=[-1,1])
{
    translate([220*i,3,25])
    {
        rotate([-90,90 + i * 90,0])
        {
            DH_GearBase();
            translate([0,0,14])
                DH_SlidingTop();
        }
    }
}

/*
translate([-178,17,25])
{
    rotate([-90,0,0])
    {
        DH_MotorMount();
    }
}
*/
// Gear plate lower
translate([199,28,25])
{
    rotate([-90,0,0])
    {
        DH_GearHolder();
    }
}
// Timing belt gear
translate([199,32,25])
{
    rotate([-90,0,0])
    {
        GT2_Gear_NoTeeth();
    }
}
// Gear plate upper
translate([199,43,25])
{
    rotate([-90,0,0])
    {
        DH_GearHolder();
    }
}
/*
translate([0,78,25])
    rotate([90,0,90])
        DH_LeadScrewNutHolder();
*/

/*
// Opeing spacers
for(i=[-1,1])
{
    translate([i*8,3,25])
        rotate([-90,0,0])
            cylinder(d=5,h=100);
}
*/
/*
// Opeing nut
translate([0,106.5,25])
    rotate([90,0,0])
        ls_nut();
*/

/*
// Opeing rod and mount
for(i=[-1,1])
    translate([205*i,3,25])
        rotate([-90,0,0])
        {
            // ZH8();
            cylinder(d=8,h=400);
        }
*/
/*
// Opeing rod bearings
for(j=[100,300])
    for(i=[-1,1])
        translate([200*i,j,36])
            rotate([180,0,90])
                SC8UU();
*/
/*
// Rack mount plate
translate([-225,84,47])
    cube([450,240,5]);
*/
    
    
    
// MODULES
module LM6UU_copper()
{
    color("DarkOrange")
    {
        translate([-7.5,0,0])
        {
            rotate([0,90,0])
            {
                difference()
                {
                    cylinder(d=10,h=15);
                    translate([0,0,-0.001])
                    cylinder(d=6,h=15.002);
                }
            }
        }
    }
}