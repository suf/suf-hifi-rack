$fn=180;
module DH_BeltSpanner()
{
    color("red")
    {
        difference()
        {
            union()
            {
                translate([0,-8.5,1])
                {
                    cube([15.9,17,5.5]);
                }
                translate([6.5,-8.5,6.5])
                {
                    cube([9.4,17,7]);
                }
                translate([2.5,0.85,6.5])
                {
                    cylinder(d=4,h=7);
                }
                translate([7.95,0,1])
                {
                    cube([15.9,5.5,2], center= true);
                }
            }

            translate([6.499,-0.5,6.5])
            {
                cube([9.402,2.7,7.001]);
            }
            translate([-0.001,0,3.5])
            {
                rotate([0,90,0])
                {
                    cylinder(d=3.5,h=22.5);
                }
            }
        }
    }
}

DH_BeltSpanner();
