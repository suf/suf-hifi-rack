$fn=180;
module alu_profile(xblocks,yblocks,length)
{
    p_width=20;
    p_outer_wall=1.8;
    p_inner_wall=1.2;
    p_opening=6.2;
    p_x=xblocks;
    p_y=yblocks;
    p_corner_r=1.5;
    p_center_hole_r=2.5;
    p_center_block_side=7.8;
    p_slot_width=11;
    
    p_corner_side=p_width*0.5-p_slot_width*0.5-p_outer_wall;
    linear_extrude(length)
    {
        union()
        {
            // outer wall
            difference()
            {
                minkowski()
                {
                    square([p_width*p_x-2*p_corner_r,p_width*p_y-2*p_corner_r],center=true);
                    circle(r=p_corner_r);
                }
                square([p_width*p_x-p_outer_wall*2,p_width*p_y-p_outer_wall*2],center=true);
                for(i=[1:p_x])
                {
                    translate([p_width*(p_x/-2 + i - 0.5),0])
                    {
                        square([p_opening,p_y*p_width+0.002],center=true);
                    }
                }
                for(i=[1:p_y])
                {
                    translate([0,p_width*(p_y/-2 + i - 0.5)])
                    {
                        square([p_x*p_width+0.002,p_opening],center=true);
                    }
                }
            }
            // center blocks
            for(k=[-1,1])
            {
                for(j=[0,1])
                {
                    for(i=[1:p_x])
                    {
                        translate([p_width*(p_x/-2 + i - 0.5),p_width*(p_y/-2 + j*(p_y-1) + 0.5)])
                        {
                            difference()
                            {
                                union()
                                {
                                    square([p_center_block_side,p_center_block_side],center=true);
                                    hull()
                                    {
                                        circle(d=p_inner_wall);
                                        translate([(p_slot_width + p_inner_wall)/2 *k,(p_slot_width + p_inner_wall)/2 *(-1+2*j)])
                                        {
                                            circle(d=p_inner_wall);
                                        }
                                    }
                                    hull()
                                    {
                                        translate([(p_slot_width + p_inner_wall)/2 *k,(p_slot_width + p_inner_wall)/2 *(-1+2*j)])
                                        {
                                            circle(d=p_inner_wall);
                                        }
                                        translate([(p_slot_width + p_inner_wall)/2 *k,(p_width - p_outer_wall)/2 *(-1+2*j)])
                                        {
                                            circle(d=p_inner_wall);
                                        }
                                    }
                                }
                                circle(r=p_center_hole_r);
                            }
                        }
                    }
                }
            }
            for(k=[-1,1])
            {
                for(j=[0,1])
                {
                    for(i=[1:p_y])
                    {
                        translate([p_width*(p_x/-2 + j*(p_x-1) + 0.5),p_width*(p_y/-2 + i - 0.5)])
                        {
                            difference()
                            {
                                union()
                                {
                                    square([p_center_block_side,p_center_block_side],center=true);
                                    hull()
                                    {
                                        circle(d=p_inner_wall);
                                        translate([(p_slot_width + p_inner_wall)/2 *(-1+2*j),(p_slot_width + p_inner_wall)/2 *k])
                                        {
                                            circle(d=p_inner_wall);
                                        }
                                    }
                                    hull()
                                    {
                                        translate([(p_slot_width + p_inner_wall)/2 *(-1+2*j),(p_slot_width + p_inner_wall)/2 *k])
                                        {
                                            circle(d=p_inner_wall);
                                        }
                                        translate([(p_width - p_outer_wall)/2 *(-1+2*j),(p_slot_width + p_inner_wall)/2 *k])
                                        {
                                            circle(d=p_inner_wall);
                                        }
                                    }
                                }
                                circle(r=p_center_hole_r);
                            }
                        }
                    }
                }
            }
            // corner blocks
            for(i=[-1,1])
            {
                for(j=[-1,1])
                {
                    translate([(p_width*p_x/2 - p_outer_wall - p_corner_side/2)*i,(p_width*p_y/2 - p_outer_wall - p_corner_side/2)*j])
                    {
                        square([p_corner_side, p_corner_side],center=true);
                    }
                }
            }
        }
    }
}

alu_profile(2,1,30);