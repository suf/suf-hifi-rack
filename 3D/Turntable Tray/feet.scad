$fn=360;

difference()
{
    union()
    {
        cylinder(d=40,h=5);
        cylinder(d=24,h=10);
    }
    translate([0,0,-0.001])
    {
        cylinder(d=5,h=10);
    }
    translate([0,0,5])
    {
        cylinder(d1=0.001,d2=10,h=5.002);
    }
}