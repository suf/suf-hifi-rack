$fn=180;

difference()
{
    translate([0,0,8.75])
    {
        minkowski()
        {
            cube([31,31,17.5],center=true);
            cylinder(d=11,h=17.5);
        }
    }
    translate([0,0,-0.001])
        cylinder(d=34,h=35.002);
    translate([0,-11,-0.001])
        cube([21.001,22,35.002]);
    
    for(i=[-1,1])
        for(j=[-1,1])
            translate([i*15.5,j*15.5,-0.001])
                cylinder(d=4,h=35.002);
}