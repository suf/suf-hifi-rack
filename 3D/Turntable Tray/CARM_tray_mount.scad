$fn=180;

difference()
{
    cylinder(d=25,h=8);
    translate([0,0,5])
        cylinder(d=7.5*tan(30)*2, h=3.001, $fn=6);
    translate([0,0,-0.001])
        cylinder(d=4.5, h=5.002);
}