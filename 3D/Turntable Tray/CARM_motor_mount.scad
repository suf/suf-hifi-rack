$fn=180;

difference()
{
    hull()
    {
        translate([0,0,1.5])
        {
            minkowski()
            {
                cube([31,31,3],center=true);
                cylinder(d=11,h=3);
            }
        }
        translate([36.5,0,0])
            cylinder(d=25,h=6);
    }
    translate([36.5,0,-0.001])
        cylinder(d=4.5,h=6.002);
    translate([0,0,-0.001])
        cylinder(d=30,h=6.002);    
    for(i=[-1,1])
        for(j=[-1,1])
            translate([i*15.5,j*15.5,-0.001])
                cylinder(d=4,h=6.002);
}