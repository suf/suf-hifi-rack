$fn=180;

difference()
{
    minkowski()
    {
        translate([0,0,0.625])
            cube([40,40,1.25], center=true);
        cylinder(d=6,h=1.25);
    }
    for(i=[-1,1])
        for(j=[-1,1])
        {
            translate([20*i,20*j,-0.001])
                cylinder(d=3.5,h=2.502);
            translate([20*i,20*j,-0.25])
                cylinder(d1=0.001,d2=5.5,h=2.752);
        }
    translate([0,0,-0.001])
    {
            intersection()
            {
                cylinder(d=16.5,h=2.502);
                translate([-7.5,-9,0])
                    cube([15,18,2.502]);
            }
    }
}