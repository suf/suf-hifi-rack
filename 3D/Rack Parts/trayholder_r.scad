$fn=360;
module trayholder_r()
{
    difference()
    {
        union()
        {
            hull()
            {
                cylinder(d=16,h=23);
                translate([-10,10,0])
                    cube([20,0.001,23]);
            }
            translate([8,10,0])
                cube([2,3,23]);
        }
        translate([0,0,-0.001])
            cylinder(d=6,h=23.002);
         translate([-10.001,-8.001,21])
            cube([6.001,21.002,2.001]);
         translate([4,-8.001,21])
            cube([6.001,21.002,2.001]);
         translate([0,0,21])
            cylinder(d=11.5,h=2.001);
          
    }
}

trayholder_r();