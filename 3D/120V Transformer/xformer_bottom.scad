$fn=180;
use <../primitives/HexSpacer.scad>
module toroid()
{
    color("red")
    {
        difference()
        {
            cylinder(d=100, h=50);
            translate([0,0,-0.001])
                cylinder(d=40,h=50.002);
        }
    }
}

difference()
{
    union()
    {
        difference()
        {
            translate([-60,0,0])
                cube([120,200,60]);
            translate([-57,3,3])
                cube([114,194,57.001]);

            for(i=[0:4])
            {
                translate([-50 + i*22,-0.001,18.85])
                    cube([2.3,3.002,22.3]);
                translate([-50 + i*22,-0.001,21])
                    cube([12,3.002,18]);
                translate([-50 + i*22+ 4.7,2,17])
                {
                    rotate([15,0,0])
                    {
                        cube([7.3,3.002,5]);
                    }
                }
                translate([-50 + i*22+ 4.7,2,43])
                {
                    rotate([-15,0,0])
                    {
                        translate([0,0,-5])
                        {
                            cube([7.3,3.002,5]);
                        }
                    }
                }
            }
            for(i=[1:3])
            {
                for(j=[-1,1])
                {
                    for(k=[1:5])
                    {
                        translate([58.5*j,20+40*i,k*10])
                        {
                            cube([3.002,15,3], center=true);
                        }
                    }
                }
            }
            translate([-20,198.5,30])
            {
                cube([27.1,3.002,31.3], center=true);
            }
            for(i=[-1,1])
            {
                translate([-20+8*i,198.5,30])
                {
                    cube([6.4,3.002,32], center=true);
                }
            }
        }
        translate([0,100,3])
            cylinder(d1=35,d2=25,h=8);
        for(i=[1,4])
        {
            for(j=[-1,1])
            {
                translate([j*52,i*40,0])
                {
                    cylinder(d=12,h=57);
                    translate([-3+3*j,-6,0])
                    {
                        cube([6,12,57]);
                    }
                }
            }
        }
        
    }
    translate([0,100,-1])
    {
        cylinder(d1=28,d2=18,h=8);
        cylinder(d=6.5,h=12.001);
    }
    for(i=[1,4])
    {
        for(j=[-1,1])
        {
            translate([j*52,i*40,-0.001])
            {
                cylinder(d=5,h=57.002);
                HexSpacer(7.2,4,5);
            }
        }
    }
}


for(i=[0:3])
{
    translate([-33 + i*22,3,0])
    {
        cylinder(d=5,h=57);
    }
}
for(i=[2:3])
{
    for(j=[-1,1])
    {
        translate([57*j,i*40,0])
        {
            cylinder(d=5,h=57);
        }
    }
}
for(i=[-1:1])
{
    translate([i*40,197,0])
    {
        cylinder(d=5,h=57);
    }
}


/*
translate([0,100,3])
    toroid();
*/
