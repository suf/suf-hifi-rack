$fn=180;
use <../primitives/HexSpacer.scad>
module toroid()
{
    color("red")
    {
        difference()
        {
            cylinder(d=100, h=50);
            translate([0,0,-0.001])
                cylinder(d=40,h=50.002);
        }
    }
}

difference()
{
    union()
    {
        difference()
        {
            translate([-56.75,3.25,0])
                cube([113.5,193.5,3]);
            for(i=[1:3])
            {
                for(k=[-5:5])
                {
                    translate([10*k,20+40*i,1.5])
                    {
                        cube([3,15,3.002], center=true);
                    }
                }
            }
        }
    }
    for(i=[1,4])
    {
        for(j=[-1,1])
        {
            translate([j*52,i*40,3.001 - 4.5])
            {
                cylinder(d1=0.001, d2=9,h=4.5);
                cylinder(d=5,h=4.5);
            }
        }
    }
}

/*
translate([0,100,3])
    toroid();
*/
