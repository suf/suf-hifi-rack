/*
 Name:		TTS.ino
 Created:	12/21/2019 7:00:53 AM
 Author:	zoli
*/

#include <Arduino.h>
#include "main.h"
#include <CapacitiveSensor.h>
#include <TMCStepper.h>
#include <TMCStepper_UTILITY.h>

CapacitiveSensor TouchSense = CapacitiveSensor(TOUCH_PIN_CHARGE,TOUCH_PIN_SENSE);

unsigned long touch_time_val;
String buff_cmd;
bool lock = false;
bool cmd_open = false;
bool cmd_close = false;
long touchVal;
bool touch = false;
bool touch_released = false;
unsigned long touch_interval_counter;

unsigned long slow_count;
bool slow_mode;

// the setup function runs once when you press reset or power the board
void setup()
{
	// Setup serial
	Serial.begin(115200);
	delay(1000);
	buff_cmd = "";
	// Setup Touch Sensor
	touch_interval_counter = millis();
	// Setup Endstops
	pinMode(LIMIT_OPEN, INPUT);
	pinMode(LIMIT_CLOSE, INPUT);
	// Setup motor
	pinMode(STEPPER_EN, OUTPUT);
	digitalWrite(STEPPER_EN, HIGH);
	pinMode(STEPPER_DIR, OUTPUT);
	pinMode(STEPPER_STEP, OUTPUT);
	slow_count = 0;
	slow_mode = false;
	//setup timer2 for stepping
	// interrupt at 8kHz
	TCCR2A = 0;// set entire TCCR2A register to 0
	TCCR2B = 0;// same for TCCR2B
	TCNT2 = 0;//initialize counter value to 0
	// set compare match register for 8khz increments
	OCR2A = 249;// = (16*10^6) / (8000*8) - 1 (must be <256)
	// turn on CTC mode
	TCCR2A |= (1 << WGM21);
	// Set CS21 bit for 8 prescaler
	TCCR2B = (1 << CS21);
	// enable timer compare interrupt
	TIMSK2 |= (1 << OCIE2A);
}

// the loop function runs over and over again until power down or reset
void loop()
{
	// read serial char if available
	byte ch;
	if (Serial.available())
	{
		ch = Serial.read();
		if (ch == 13)
		{
			if(buff_cmd == "lock")
			{
				lock = true;
			}
			if(buff_cmd == "unlock")
			{
				lock = false;
			}
			if (buff_cmd == "open" && !lock)
			{
				if (!digitalRead(LIMIT_OPEN))
				{
					Serial.println("opening");
					cmd_open = true;
					cmd_close = false;
				}
			}
			if (buff_cmd == "close" && !lock)
			{
				if (!digitalRead(LIMIT_CLOSE))
				{
					Serial.println("closing");
					cmd_open = false;
					cmd_close = true;
				}
			}
			if (buff_cmd == "stop" && !lock)
			{
				cmd_open = false;
				cmd_close = false;
			}
			if (buff_cmd == "state")
			{
				Serial.print("CMD Open: ");
				Serial.println(cmd_open);
				Serial.print("CMD Close: ");
				Serial.println(cmd_close);
				Serial.print("Limit open: ");
				Serial.println(digitalRead(LIMIT_OPEN));
				Serial.print("Limit close: ");
				Serial.println(digitalRead(LIMIT_CLOSE));
				Serial.print("Lock: ");
				Serial.println(lock);
				/*
				Serial.print("Touch: ");
				Serial.println(touch);
				*/
				if (cmd_open)
				{
					Serial.println("opening");
				}
				if (cmd_close)
				{
					Serial.println("closing");
				}
				if (digitalRead(LIMIT_OPEN) && !cmd_open && !cmd_close)
				{
					Serial.println("open");
				}
				if (digitalRead(LIMIT_CLOSE) && !cmd_open && !cmd_close)
				{
					Serial.println("closed");
				}
			}
			buff_cmd = "";
		}
		else
		{
			buff_cmd += (char)ch;
		}
	}
	// check touch sensor
	if (touch_interval_counter + TOUCH_SAMPLE_INTERVAL < millis())
	{
		touchVal = TouchSense.capacitiveSensor(30);
		touch_interval_counter = millis();
		if (touchVal < TOUCH_THRESHOLD)
		{
			touch_released = true;
		}
		if (touchVal > TOUCH_THRESHOLD && touch_released)
		{
			touch = !lock;  // touch will only be true if the unit is in unlocked state
			touch_released = false;
		}
		// Serial.println(touchVal);
	}
	// if it is opening
	if (touch && cmd_open)
	{
		// stop
	    Serial.println("stop opening - touch");
		cmd_open = false;
		touch = false;
	}
	// if it is closing
	if (touch && cmd_close)
	{
		// stop
	    Serial.println("stop closing - touch");
		cmd_close = false;
		touch = false;
	}
	// if closed - open
	if (touch && digitalRead(LIMIT_CLOSE))
	{
		Serial.println("start opening - touch");
		cmd_open = true;
		touch = false;
	}
	// if open - close
	if (touch && digitalRead(LIMIT_OPEN))
	{
		Serial.println("start closing - touch");
		cmd_close = true;
		touch = false;
	}
	// check endstops
	if (cmd_open && digitalRead(LIMIT_OPEN))
	{
		Serial.println("opening slowdown - endstop");
		slow_mode = true;
		// change timer prescaler
		// Set CS20, CS21 bit for 32 prescaler
		TCCR2B = (1 << CS21) | (1 << CS20);
		if (slow_count > SLOW_OPEN_STEPS)
		{
			// Stop
			digitalWrite(STEPPER_EN, HIGH);
			cmd_open = false;
			// Set CS21 bit for 8 prescaler
			TCCR2B = (1 << CS21);
			slow_mode = false;
		}
	}
	if (cmd_close && digitalRead(LIMIT_CLOSE))
	{
		Serial.println("closing slowdown - endstop");
		slow_mode = true;
		// change timer prescaler
		// Set CS20, CS21 bit for 32 prescaler
		TCCR2B = (1 << CS21) | (1 << CS20);
		if (slow_count > SLOW_CLOSE_STEPS)
		{
			// Stop
			digitalWrite(STEPPER_EN, HIGH);
			cmd_close = false;
			// Set CS21 bit for 8 prescaler
			TCCR2B = (1 << CS21);
			slow_mode = false;
		}
	}
	if (!slow_mode)
	{
		slow_count = 0;
	}
	// control the motor
	digitalWrite(STEPPER_DIR, cmd_open);
	digitalWrite(STEPPER_EN, !(cmd_open || cmd_close));
}

ISR(TIMER2_COMPA_vect)
{
	digitalWrite(STEPPER_STEP, !digitalRead(STEPPER_STEP));
	if (slow_mode)
	{
		slow_count++;
	}
}
