/*
 Name:		Touch_Test.ino
 Created:	1/24/2021 10:11:48 PM
 Author:	zoli
*/
#include <Arduino.h>
#include "main.h"
#include <CapacitiveSensor.h>

CapacitiveSensor TouchSense = CapacitiveSensor(TOUCH_PIN_CHARGE, TOUCH_PIN_SENSE);
long touchVal;
// bool touch = false;
// bool touch_released = false;
unsigned long touch_interval_counter;

// the setup function runs once when you press reset or power the board
void setup()
{
	// Serial port setup
	Serial.begin(115200);
	delay(1000);
	// Setup Touch Sensor
	touch_interval_counter = millis();
}

// the loop function runs over and over again until power down or reset
void loop()
{
	// check touch sensor
	if (touch_interval_counter + TOUCH_SAMPLE_INTERVAL < millis())
	{
		touchVal = TouchSense.capacitiveSensor(30);
		touch_interval_counter = millis();
		/*
		if (touchVal < TOUCH_THRESHOLD)
		{
			touch_released = true;
		}
		if (touchVal > TOUCH_THRESHOLD && touch_released)
		{
			// touch = !lock;  // touch will only be true if the unit is in unlocked state
			touch = true;
			touch_released = false;
		}
		*/
		Serial.println(touchVal);
	}
}
