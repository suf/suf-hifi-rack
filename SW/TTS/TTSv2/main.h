#pragma once
#define STEPPER_EN  3
#define STEPPER_DIR 5
#define STEPPER_STEP  2
#define LIMIT_OPEN  7
#define LIMIT_CLOSE 10

// number of steps needed after the endstop signaled in slow mode to fully open/close the lid
#define SLOW_CLOSE_STEPS 320
#define SLOW_OPEN_STEPS 320

#define TOUCH_PIN_CHARGE 4
#define TOUCH_PIN_SENSE 6
#define TOUCH_SAMPLE_INTERVAL 350
#define TOUCH_THRESHOLD 1200

#define STEPPER_TXD 8 // RXD1 on the schematics
#define STEPPER_RXD 9 // TXD1 on the schematics
#define R_SENSE 0.11f
#define STEPPER_ADDR 3
#define STEPPER_MS 16
#define STEPPER_CURRENT 1000
#define DIR_OPEN 0
#define DIR_CLOSE 1

#define PULLEY_TEETH 20
#define TOOTH_DISTANCE 2
#define STEPS_PER_REVOLUTION 200
#define STEPS_PER_MM (STEPS_PER_REVOLUTION * STEPPER_MS / PULLEY_TEETH / TOOTH_DISTANCE)

#define OPEN_DISTANCE 300
#define HOME_OFFSET -2
#define MAX_DISTANCE 350	// Maximum movement (used when we expect the movement stopped by the endstops)

#define STATE_UNDEFINED 0
#define STATE_OPENING 1
#define STATE_OPEN 2
#define STATE_CLOSING 3
#define STATE_CLOSE 4
#define STATE_OPENING_STOPPED 5
#define STATE_CLOSING_STOPPED 6

#define SERIAL_REPORT_STATUS

#define CMD_LOCK 0
#define CMD_UNLOCK 1
#define CMD_CLOSE 2
#define CMD_OPEN 3
#define CMD_HOME 4
#define CMD_STATUS 5
#define CMD_STOP 6
