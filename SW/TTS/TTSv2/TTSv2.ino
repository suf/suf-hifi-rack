/*
 Name:		TEST1.ino
 Created:	12/21/2019 7:01:24 AM
 Author:	zoli
*/

#include <Arduino.h>
#include "main.h"
#include <TMCStepper.h>
#include <TMCStepper_UTILITY.h>
#include <AccelStepper.h>
#include <CapacitiveSensor.h>

TMC2209Stepper driver = TMC2209Stepper(STEPPER_RXD, STEPPER_TXD, R_SENSE, STEPPER_ADDR);
AccelStepper stepper = AccelStepper(stepper.DRIVER, STEPPER_STEP, STEPPER_DIR);

CapacitiveSensor TouchSense = CapacitiveSensor(TOUCH_PIN_CHARGE, TOUCH_PIN_SENSE);
long touchVal;
bool touch = false;
bool touch_released = false;
unsigned long touch_interval_counter;

int state = STATE_UNDEFINED;
bool IsHoming;
bool IsHomed = false;
bool IsClosingStopSignaled = false;

volatile bool EnableRun = false;

char *states[] = { "unknown", "opening", "open", "closing", "close", "stopped (opening)", "stopped (closing)" };
String cmd_list[8] = { "lock", "unlock", "close", "open", "home", "status", "stop", "" };


String buff_cmd;
byte ch;
bool lock = false;

// the setup function runs once when you press reset or power the board
void setup()
{
	// Sensor settings
	pinMode(LIMIT_OPEN, INPUT);
	pinMode(LIMIT_CLOSE, INPUT);

	// Setup motor pins
	pinMode(STEPPER_EN, OUTPUT);
	// digitalWrite(STEPPER_EN, LOW);
	pinMode(STEPPER_DIR, OUTPUT);
	pinMode(STEPPER_STEP, OUTPUT);

	// Stepper driver settings (TMC2209)
	driver.begin();
	driver.toff(5);                 // Enables driver in software
	driver.rms_current(STEPPER_CURRENT);        // Set motor RMS current
	driver.microsteps(STEPPER_MS);          // Set microsteps to 1/16th

	//driver.en_spreadCycle(false);   // Toggle spreadCycle on TMC2208/2209/2224
	driver.pwm_autoscale(true);     // Needed for stealthChop

	// Stepper controller settings (AccelStepper)
	stepper.setMaxSpeed(100 * STEPS_PER_MM);
	stepper.setAcceleration(1000 * STEPS_PER_MM);
	stepper.setEnablePin(STEPPER_EN);
	stepper.setPinsInverted(true, false, true);
	stepper.disableOutputs();

	//setup timer2 for stepping
	// interrupt at 8kHz
	TCCR2A = 0;// set entire TCCR2A register to 0
	TCCR2B = 0;// same for TCCR2B
	TCNT2 = 0;//initialize counter value to 0
	// set compare match register for 8khz increments
//	OCR2A = 249;// = (16*10^6) / (8000*8) - 1 (must be <256)
	OCR2A = 124;// = (16*10^6) / (8000*8) - 1 (must be <256)
	// turn on CTC mode
	TCCR2A |= (1 << WGM21);
	// Set CS21 bit for 8 prescaler
	TCCR2B |= (1 << CS21);
	// TCCR2B |= (1 << CS21) | (1 << CS20);
	// enable timer compare interrupt
	TIMSK2 |= (1 << OCIE2A);

	// Serial port setup
	Serial.begin(115200);
	delay(1000);

	// move to closed position
	Home();
	// Setup Touch Sensor
	touch_interval_counter = millis();

}

// the loop function runs over and over again until power down or reset
void loop()
{
	int i;
	if (digitalRead(LIMIT_CLOSE) != 0)
	{
		if (stepper.distanceToGo() < (HOME_OFFSET * 2 - 1) * (STEPS_PER_MM / 2))
		{
			// Emergency stop
			EnableRun = false;
			// Set the home position with offset
			stepper.setCurrentPosition(HOME_OFFSET * -1 * STEPS_PER_MM);
			// go to home
			stepper.runToNewPosition(0);
			EnableRun = true;
			IsHomed = true;
			state = STATE_CLOSE;
#ifdef SERIAL_REPORT_STATUS
			SerialReportState();
#endif // SERIAL_REPORT_STATUS
		}
	}
	if (stepper.distanceToGo() == 0)
	{
		stepper.disableOutputs();
		switch (state)
		{
		case STATE_CLOSING:
			state = STATE_CLOSE;
#ifdef SERIAL_REPORT_STATUS
			SerialReportState();
#endif // SERIAL_REPORT_STATUS
			break;
		case STATE_OPENING:
			state = STATE_OPEN;
#ifdef SERIAL_REPORT_STATUS
			SerialReportState();
#endif // SERIAL_REPORT_STATUS
			break;
		default:
			break;
		}
	}
	// check touch sensor
	if (touch_interval_counter + TOUCH_SAMPLE_INTERVAL < millis())
	{
		touchVal = TouchSense.capacitiveSensor(30);
		touch_interval_counter = millis();
		if (touchVal < TOUCH_THRESHOLD)
		{
			touch_released = true;
		}
		if (touchVal > TOUCH_THRESHOLD && touch_released)
		{
			// touch = !lock;  // touch will only be true if the unit is in unlocked state
			touch = true;
			touch_released = false;
		}
		// Serial.println(touchVal);
	}
	if (touch)
	{
#ifdef SERIAL_REPORT_STATUS
		Serial.println("sensor touched");
#endif // SERIAL_REPORT_STATUS
		switch (state)
		{
			case STATE_OPEN:
			case STATE_OPENING_STOPPED:
				Close();
				break;
			case STATE_CLOSE:
			case STATE_CLOSING_STOPPED:
				Open();
				break;
			case STATE_OPENING:
				stepper.stop();
				state = STATE_OPENING_STOPPED;
#ifdef SERIAL_REPORT_STATUS
				SerialReportState();
#endif // SERIAL_REPORT_STATUS
				break;
			case STATE_CLOSING:
				stepper.stop();
				state = IsHomed ? STATE_CLOSING_STOPPED : STATE_OPENING_STOPPED;
#ifdef SERIAL_REPORT_STATUS
				SerialReportState();
#endif // SERIAL_REPORT_STATUS
				break;
			default:
				break;
		}
		touch = false;
	}

	if (Serial.available())
	{
		ch = Serial.read();
		if (ch == 13)
		{
			for (i = 0; i < 8; i++)
			{
				if (buff_cmd == cmd_list[i])
				{
					break;
				}
			}
			switch (i)
			{
				case CMD_LOCK:
					lock = true;
					break;
				case CMD_UNLOCK:
					lock = false;
					break;
				case CMD_CLOSE:
					Close();
					break;
				case CMD_OPEN:
					Open();
					break;
				case CMD_HOME:
					Home();
					break;
				case CMD_STATUS:
					Serial.print(states[state]);
					break;
				case CMD_STOP:
					if (state == STATE_CLOSING)
					{
						stepper.stop();
						state = STATE_CLOSING_STOPPED;
					}
					if (state == STATE_OPENING)
					{
						stepper.stop();
						state = STATE_OPENING_STOPPED;
					}
					break;
				default:
					break;
			}
			buff_cmd = "";
		}
		else
		{
			buff_cmd += (char)ch;
		}
	}

}

void Close()
{
	if (!lock)
	{
		if (IsHomed)
		{
			stepper.moveTo(0);
			stepper.enableOutputs();
			state = STATE_CLOSING;
#ifdef SERIAL_REPORT_STATUS
			SerialReportState();
#endif // SERIAL_REPORT_STATUS
			EnableRun = true;
		}
		else
		{
			Home();
		}
	}
}

void Open()
{
	if (!lock)
	{
		if (IsHomed)
		{
			stepper.moveTo(OPEN_DISTANCE * STEPS_PER_MM);
			stepper.enableOutputs();
			state = STATE_OPENING;
#ifdef SERIAL_REPORT_STATUS
			SerialReportState();
#endif // SERIAL_REPORT_STATUS
			EnableRun = true;
		}
	}
}

void Home()
{
	if (!lock)
	{
#ifdef SERIAL_REPORT_STATUS
		Serial.println("homing");
#endif // SERIAL_REPORT_STATUS
		EnableRun = false;
		if (digitalRead(LIMIT_CLOSE) != 0)
		{
			// out of range
			if (stepper.currentPosition() > STEPS_PER_MM || stepper.currentPosition() < STEPS_PER_MM * -1)
			{
				stepper.setCurrentPosition(0);
			}
			IsHomed = true;
			state = STATE_CLOSE;
		}
		else
		{
			stepper.move(MAX_DISTANCE * -1 * STEPS_PER_MM);
			state = STATE_CLOSING;
#ifdef SERIAL_REPORT_STATUS
			SerialReportState();
#endif // SERIAL_REPORT_STATUS
			stepper.enableOutputs();
		}
		EnableRun = true;
	}
}

void SerialReportState()
{
	Serial.println(states[state]);
}


ISR(TIMER2_COMPA_vect)
{
  if(EnableRun)
  {
	  stepper.run();
  }
}
