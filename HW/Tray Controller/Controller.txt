Item	Qty	Reference(s)	Value	LibPart	Footprint	Supplier	Link
1	3	C1, C3, C4	100nF	Device:C_Small	Capacitors_SMD:C_0603_HandSoldering	Book	
2	1	C2	470uF/25V	Device:CP	Capacitors_SMD:CP_Elec_10x10	Lomex	https://lomex.hu/hu/webshop/#/search,91-00-78/stype,1
3	2	C5, C6	22pF	Device:C_Small	Capacitors_SMD:C_0603_HandSoldering	Book	
4	1	D1	SS14	Device:D_Schottky	Diodes_SMD:D_SMA	Lomex	https://lomex.hu/hu/webshop/#/search,83-02-97/stype,1
5	1	D2	MBR0520LT1G	Device:D_Schottky	Diodes_SMD:D_SOD-123	Lomex	https://lomex.hu/hu/webshop/#/search,83-00-05/stype,1
6	1	J1	ESTOP_OPEN	conn:Conn_01x03	Connectors_JST:JST_EH_B03B-EH-A_03x2.50mm_Straight	Shelf	
7	1	J2	ESTOP_CLOSE	conn:Conn_01x03	Connectors_JST:JST_EH_B03B-EH-A_03x2.50mm_Straight	Shelf	
8	1	J3	PWR/SER	conn:Conn_01x05	suf_connector_ncw:CONN_NCW396-05S	Shelf	
9	1	J4	TOUCH	conn:Conn_01x01	Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm	Shelf	
10	1	J5	Stepper	conn:Conn_01x04	Connectors_JST:JST_EH_B04B-EH-A_04x2.50mm_Straight	Shelf	
11	1	J6	PWR LED	conn:Conn_01x02	Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm	Shelf	
12	4	J7, J10, J12, J13	Grove	conn:Conn_01x04	Connectors_JST:JST_PH_B4B-PH-K_04x2.00mm_Straight	Shelf	
13	1	J8	GND	conn:Conn_01x06	Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm	Shelf	
14	1	J9	SIGNAL	conn:Conn_01x06	Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm	Shelf	
15	1	J11	VCC	conn:Conn_01x06	Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm	Shelf	
16	1	J14	ISP	atmel:AVR-ISP-6	Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm	Shelf	
17	1	J15	Serial	conn:Conn_01x03	Connectors_JST:JST_EH_B03B-EH-A_03x2.50mm_Straight	Shelf	
18	1	JP1	MS2	Device:Jumper_NC_Small	Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm	Shelf	
19	1	JP2	MS1	Device:Jumper_NC_Small	Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm	Shelf	
20	2	R1, R2	100K	Device:R	Resistors_SMD:R_0603_HandSoldering	Book	
21	1	R3	1K	Device:R	Resistors_SMD:R_0603_HandSoldering	Book	
22	2	R4, R8	1M	Device:R	Resistors_SMD:R_0603_HandSoldering	Book	
23	1	R5	510	Device:R	Resistors_SMD:R_0603_HandSoldering	Book	
24	2	R6, R7	4,7K	Device:R	Resistors_SMD:R_0603_HandSoldering	Book	
25	1	R9	10K	Device:R	Resistors_SMD:R_0603_HandSoldering	Book	
26	1	U1	BIGTREETECH_TMC2209	suf_module:BIGTREETECH_TMC2209	suf_module:POLOLU_STEPPER	AliExpress	
27	1	U2	ATMEGA328-AU	atmel:ATMEGA328-AU	Housings_QFP:TQFP-32_7x7mm_Pitch0.8mm	Shelf	
28	1	Y1	12MHz	Device:Crystal_Small	Crystals:Crystal_SMD_HC49-SD	Lomex	https://lomex.hu/hu/webshop/#/search,40-01-25/stype,1
